<?php
	# Here the data will me handled, checked and stored.

	function isClientBlocked($connection, $key) {
		$query = "SELECT * FROM clients WHERE client_key='$key'";
		$result = mysqli_query($connection, $query);
		$row = mysqli_fetch_assoc($result);
		$block = $row["block"];

		if ($block == "1") {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	# Getting JSON string passed through letty.js.
	$attributes = $_POST["attributes"];

	# Converting JSON string into JSON Object.
	$attributesDecoded = json_decode($attributes, true);

	# Oppening connection with the database.
	$connection = mysqli_connect("localhost", "root", "rootUser2016");
	if (!$connection) {
		die("<p>The database server is not available</p>" .
		"<p>Error code: " . mysqli_connect_errno() .
		": " . mysqli_connect_error() . "</p>");
	}

	# Selecting a database.
	$database  = mysqli_select_db($connection, "sc");
	if (!$database) {
		die("<p>It was not possible to select the database</p>" .
		"<p>Error code: " . mysqli_errno($connection) .
		": " . mysqli_error($connection) . "<br />");
	}

	# Getting attributes from JSON Object.
	$userAgent = $attributesDecoded['userAgent'];
	$product = $attributesDecoded['product'];
	$productSub = $attributesDecoded['productSub'];
	$cookieEnabled = $attributesDecoded['cookieEnabled'];
	$vendor = $attributesDecoded['vendor'];
	$platform = $attributesDecoded['platform'];
	$language = $attributesDecoded['language'];
	$languages = $attributesDecoded['languages'];
	$javaEnabled = $attributesDecoded['javaEnabled'];
	$appName = $attributesDecoded['appName'];
	$appCodeName = $attributesDecoded['appCodeName'];
	$appVersion = $attributesDecoded['appVersion'];
	$oscpu = $attributesDecoded['oscpu'];
	$maxTouchPoints = $attributesDecoded['maxTouchPoints'];
	$colorDepth = $attributesDecoded['colorDepth'];
	$pixelDepth = $attributesDecoded['pixelDepth'];
	$width = $attributesDecoded['width'];
	$height = $attributesDecoded['height'];
	$plugins = $attributesDecoded['plugins'];
	$mimeTypes = $attributesDecoded['mimeTypes'];
	$device = $attributesDecoded['device'];
	$key = $attributesDecoded['key'];
	$user = $attributesDecoded['user'];

	# Building query. This query is for the first access of a client.
	$query_clients = "INSERT INTO clients (userAgent,product,productSub,
										cookieEnabled,vendor,platform,language,languages,
										javaEnabled,appName, appCodeName,appVersion,oscpu,
										maxTouchPoints,colorDepth,pixelDepth,width,height,plugins,
										mimeTypes,device,client_key,user,block) VALUES
                    ('$userAgent','$product','$productSub', '$cookieEnabled',
								   	'$vendor', '$platform','$language', '$languages',
								   	'$javaEnabled', '$appName','$appCodeName', '$appVersion',
								   	'$oscpu', '$maxTouchPoints','$colorDepth', '$pixelDepth',
								   	'$width', '$height','$plugins', '$mimeTypes',
								   	'$device','$key','$user','0')";

	# Building query. This query is for second access of a client
  #(or for a hashing collision).
	$query_dup_clients = "INSERT INTO dup_clients (userAgent,product,productSub,
												cookieEnabled,vendor,platform,language,languages,
												javaEnabled, appName, appCodeName,appVersion,oscpu,
			 					   		  maxTouchPoints,colorDepth,pixelDepth,width,
			 					   		  height,plugins,mimeTypes,device,dpclients_key,
                        dpclients_user) VALUES ('$userAgent','$product',
												'$productSub', '$cookieEnabled','$vendor','$platform',
												'$language','$languages','$javaEnabled','$appName',
												'$appCodeName','$appVersion','$oscpu',
												'$maxTouchPoints','$colorDepth', '$pixelDepth','$width',
												'$height','$plugins', '$mimeTypes','$device','$key',
												'$user')";


	$result = mysqli_query($connection, $query_clients);

	if ($result) {

    echo "1";
	} else {

		$result = mysqli_query($connection, $query_dup_clients);
		if (!isClientBlocked($connection, $key) && $result) {

      echo "2";
		} elseif ($result) {
      echo "3";
		} else {

			echo "4";
		}
	}

	mysqli_close($connection);
?>
