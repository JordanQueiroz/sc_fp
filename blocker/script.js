
// Verifica se a página foi renderizada completamente.
$(document).ready(function() {

  $.post('users.php', {}, function(data){

    var users = [];

    // Decoding PHP's JSON response.
    var users_array = JSON.parse(data);
    for(var i = 0; i < users_array.length; i++){
      // Getting JSON objects and putting them in an array.
      users.push(JSON.parse(users_array[i]));
    }

    // Find a <table> element with id="myTable":
    var table = document.getElementById("myTable");
    //table.className="table table-striped";

    for(var i = 0; i < users.length; i++) {
      // +1 because the table's header is already set, so we don't want to override it.
      var row = table.insertRow(-1);

      // Inserting cell on the first and second position, respectively, on the new row.
      var key = row.insertCell(0);
      var user = row.insertCell(1);
      var btnCell = row.insertCell(2);

      key.innerHTML = users[i]["key"];
      user.innerHTML = users[i]["user"];

      var btn = document.createElement("button");        // Create a <button> element

      if (users[i]["block"] == "0") {
        var t = document.createTextNode("Bloquear");       // Create a text node
        btn.className="btn btn-danger btn-block"
      } else {
        var t = document.createTextNode("Desloquear");       // Create a text node
        btn.className="btn btn-success btn-block"
      }

      btn.appendChild(t);
      btn.id=users[i]["key"];
      btn.class=users[i]["block"];
      btn.onclick = verify;
      btnCell.appendChild(btn);
    }
  });
});

function verify(e) {
  var id = e.target.getAttribute("id");
  var classe = e.target.getAttribute("class");
  var status;

  if (classe == "btn btn-danger btn-block") {
    attributes = new Object();
    attributes.key = id;
    attributes.block = "1";
    
    attributesString = JSON.stringify(attributes);

    $.post('blocker.php', {"attributes":attributesString}, function(data){
     if (data == "ok") {
        e.target.className="btn btn-success btn-block";
        e.target.innerHTML = "Desbloquear";
      }
    });
  } else {
    attributes = new Object();
    attributes.key = id;
    attributes.block = "0";
    
    attributesString = JSON.stringify(attributes);

    $.post('blocker.php', {"attributes":attributesString}, function(data){
      if (data == "ok") {
        e.target.className="btn btn-danger btn-block";
        e.target.innerHTML = "Bloquear";
      }
    });
  }
}
